#!/bin/sh

PROJECT_NAME="provo-rabbitmq"
ENV=$1
if [ -z "$ENV" ]; then
  ENV=STAGING
fi
LOWERCASE_ENV=$(echo "$ENV" | tr '[:upper:]' '[:lower:]')
BASE_PATH=$(dirname \"$0\" | tr -d '"')
NAMESPACE=provo-$LOWERCASE_ENV
OPERATOR_NAME=rabbitmq
OPERATOR_NAMESPACE=rabbitmq



echo "current cluster context:"
kubectl config current-context
kubectl get nodes
echo ""
echo "is this the right cluster? [yes|no]"
read CONFIRMATION
if [ ! "$CONFIRMATION" = "yes" ] && [ ! "$CONFIRMATION" = "y" ]; then
  exit 0
fi
echo "continuing..."


VALUES_BASE_YAML=$BASE_PATH/k8s/values.yaml
VALUES_ENV_YAML=$BASE_PATH/k8s/values.$LOWERCASE_ENV.yaml


helm template $PROJECT_NAME \
              -f $VALUES_BASE_YAML \
              -f $VALUES_ENV_YAML \
              $BASE_PATH/k8s > $BASE_PATH/deploy-template.yaml

kubectl delete -f $BASE_PATH/deploy-template.yaml



# if [ ! -z "$(kubectl get deploy -n $OPERATOR_NAMESPACE -l app.kubernetes.io/name=rabbitmq-cluster-operator,app.kubernetes.io/instance=$OPERATOR_NAME --no-headers --ignore-not-found)" ]; then
#   # https://github.com/bitnami/charts/tree/main/bitnami/rabbitmq-cluster-operator
#   helm uninstall $OPERATOR_NAME \
#     -n $OPERATOR_NAMESPACE \
#     oci://registry-1.docker.io/bitnamicharts/rabbitmq-cluster-operator 
# fi


echo "app removed!"

