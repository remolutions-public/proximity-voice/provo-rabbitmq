#!/bin/sh


IMAGE_NAME="provo-rabbitmq-user-manager"
REGISTRY_TARGET="registry.gitlab.com"


REGISTRY_PATH=$1
if [ -z $REGISTRY_PATH ]; then
  echo "REGISTRY_PATH was not provided!"
  exit 1
fi


IMAGE_VERSION=$2
if [ -z $IMAGE_VERSION ]; then
  echo "IMAGE_VERSION was not provided!"
  exit 1
fi


IMAGE_TAG=$IMAGE_VERSION-manual-build
IMAGE_PATH="$REGISTRY_TARGET/$REGISTRY_PATH/$IMAGE_NAME:$IMAGE_TAG"


docker build -t "$IMAGE_PATH" .

# echo "$REGISTRY_PW" | docker login $REGISTRY_TARGET -u $REGISTRY_USER --password-stdin
docker push "$IMAGE_PATH"


echo "pushed image $IMAGE_PATH"
