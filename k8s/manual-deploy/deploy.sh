#!/bin/sh


PROJECT_NAME="provo-rabbitmq-user-manager"
REGISTRY_TARGET="registry.gitlab.com"
BASE_PATH=$(dirname \"$0\" | tr -d '"')/..

if [ -z "$1" ]; then
  ENVIRONMENT="staging"
else
  ENVIRONMENT=$1
fi
LOWERCASE_ENV=$(echo "$ENVIRONMENT" | tr '[:upper:]' '[:lower:]')
NAMESPACE="provo-$LOWERCASE_ENV"
OPERATOR_NAME=rabbitmq
OPERATOR_NAMESPACE=rabbitmq


REGISTRY_PATH="$2"
if [ -z $REGISTRY_PATH ]; then
  echo "REGISTRY_PATH was not provided!"
  exit 1
fi

IMAGE_VERSION="$3"
if [ -z $IMAGE_VERSION ]; then
  echo "IMAGE_VERSION was not provided!"
  exit 1
fi


# create namespace if not exists
FOUND_NAMESPACE=$(kubectl get namespace $NAMESPACE --no-headers --ignore-not-found)
if [ -z "$FOUND_NAMESPACE" ]; then
  kubectl create namespace $NAMESPACE --dry-run=client -oyaml | kubectl apply -f -
fi



# create secret if not exists
SECRET_NAME=provo-rabbitmq-users
FOUND_SECRET=$(kubectl get secret $SECRET_NAME -n $NAMESPACE --no-headers --ignore-not-found)
if [ -z "$FOUND_SECRET" ]; then
  if [ -z "$PROVO_CLIENT_USER" ]; then
    PROVO_CLIENT_USER="provo_client:$(openssl rand -base64 1000 | tr "[:upper:]" "[:lower:]" | tr -cd "[:alnum:]" | tr -d "lo" | cut -c 1-32)"
  fi
  if [ -z "$PROVO_API_USER" ]; then
    PROVO_API_USER="provo_api:$(openssl rand -base64 1000 | tr "[:upper:]" "[:lower:]" | tr -cd "[:alnum:]" | tr -d "lo" | cut -c 1-32)"
  fi
  kubectl create secret generic $SECRET_NAME -n $NAMESPACE \
      --from-literal=users="$PROVO_CLIENT_USER,$PROVO_API_USER" \
      --dry-run=client -oyaml | kubectl apply -f -
fi



# install the operator if not already present
if [ -z "$(kubectl get ns $OPERATOR_NAMESPACE --no-headers --ignore-not-found)" ]; then
  kubectl create namespace $OPERATOR_NAMESPACE
fi
if [ -z "$(kubectl get deploy -n $OPERATOR_NAMESPACE -l app.kubernetes.io/name=rabbitmq-cluster-operator,app.kubernetes.io/instance=$OPERATOR_NAME --no-headers --ignore-not-found)" ]; then
  # https://github.com/bitnami/charts/tree/main/bitnami/rabbitmq-cluster-operator
  helm upgrade --install $OPERATOR_NAME \
    -n $OPERATOR_NAMESPACE -f $BASE_PATH/k8s/operator/values.yaml \
    oci://registry-1.docker.io/bitnamicharts/rabbitmq-cluster-operator 
fi
  


IMAGE_TAG=$IMAGE_VERSION-manual-build
IMAGE_NAME="$PROJECT_NAME"
IMAGE_PATH="$REGISTRY_TARGET/$REGISTRY_PATH/$IMAGE_NAME:$IMAGE_TAG"



VALUES_BASE_YAML=$BASE_PATH/values.yaml
VALUES_ENV_YAML=$BASE_PATH/values.$LOWERCASE_ENV.yaml


helm template $PROJECT_NAME -n $NAMESPACE \
              -f $VALUES_BASE_YAML \
              -f $VALUES_ENV_YAML \
              --set userManager.image=$IMAGE_PATH \
              $BASE_PATH > $BASE_PATH/deploy-template.yaml

kubectl apply -f $BASE_PATH/deploy-template.yaml


echo "app deployed!"
