#!/bin/sh


IMAGE_VERSION="1.0.18"
REGISTRY_PATH=remolutions-public/proximity-voice/provo-rabbitmq


PROJECT_NAME="provo-rabbitmq"
ENVIRONMENT=$1
if [ -z "$ENVIRONMENT" ]; then
  ENVIRONMENT=STAGING
fi
LOWERCASE_ENV=$(echo "$ENVIRONMENT" | tr '[:upper:]' '[:lower:]')
BASE_PATH=$(dirname \"$0\" | tr -d '"')
NAMESPACE=provo-$LOWERCASE_ENV



echo "current cluster context:"
kubectl config current-context
kubectl get nodes
echo ""
echo "is this the right cluster? [yes|no]"
read CONFIRMATION
if [ ! "$CONFIRMATION" = "yes" ] && [ ! "$CONFIRMATION" = "y" ]; then
  exit 0
fi
echo "continuing..."



sh k8s/manual-deploy/dockerize.sh $REGISTRY_PATH $IMAGE_VERSION


sh k8s/manual-deploy/deploy.sh $ENVIRONMENT $REGISTRY_PATH $IMAGE_VERSION

