#!/bin/sh
# HINT: should use cygwin if dev on windows

# source env vars
set -o allexport
. ./.env.dev
set +o allexport


if [ -z "$(docker volume ls -q -f "name=$RABBITMQ_VOLUME_NAME")" ]; then
  docker volume create $RABBITMQ_VOLUME_NAME
fi


docker rm -f $(docker ps -a -q --filter="name=$RABBITMQ_CONTAINER_NAME")
docker run \
    --name $RABBITMQ_CONTAINER_NAME \
    -e RABBITMQ_USERNAME=$RABBITMQ_USERNAME \
    -e RABBITMQ_PASSWORD=$RABBITMQ_PASSWORD \
    -e RABBITMQ_VHOSTS="/main /queue" \
    -e RABBITMQ_PLUGINS="rabbitmq_management,rabbitmq_mqtt" \
    -p 1883:1883 \
    -p 5672:5672 \
    -p 15672:15672 \
    -v $RABBITMQ_VOLUME_NAME:/bitnami \
    -d \
    bitnami/rabbitmq:3.9.26


# check plugins
# /opt/bitnami/rabbitmq/sbin/rabbitmq-plugins list
