#!/bin/sh


PROJECT_NAME="provo-rabbitmq"
ENV=$CI_ENVIRONMENT_NAME
if [ -z "$ENV" ]; then
  ENV=STAGING
fi
LOWERCASE_ENV=$(echo "$ENV" | tr '[:upper:]' '[:lower:]')
BASE_PATH=$(dirname \"$0\" | tr -d '"')
NAMESPACE="provo-$LOWERCASE_ENV"


mkdir -p $HOME/.kube
if [ "$ENV" = "PROD" ]; then
  KUBE_CONFIG=$KUBE_CONFIG_PROD
  NAMESPACE=$NAMESPACE_PROD
  REDIS_PASSWORD=$REDIS_PASSWORD_PROD
elif [ "$ENV" = "STAGING" ]; then
  KUBE_CONFIG=$KUBE_CONFIG_STAGING
  NAMESPACE=$NAMESPACE_STAGING
  REDIS_PASSWORD=$REDIS_PASSWORD_STAGING
fi
cp $KUBE_CONFIG $HOME/.kube/config


# create secret if not exists
SECRET_NAME=$PROJECT_NAME-secrets
FOUND_SECRET=$(kubectl get secret $SECRET_NAME -n $NAMESPACE --no-headers --ignore-not-found)
if [ -z "$FOUND_SECRET" ]; then
  if [ -z "$RABBITMQ_PASSWORD" ]; then
    RABBITMQ_PASSWORD=$(openssl rand -base64 1000 | tr "[:upper:]" "[:lower:]" | tr -cd "[:alnum:]" | tr -d "lo" | cut -c 1-16)
  fi
  kubectl create secret generic $SECRET_NAME -n $NAMESPACE \
      --from-literal=RABBITMQ_USERNAME=provo \
      --from-literal=RABBITMQ_PASSWORD=$RABBITMQ_PASSWORD \
      --dry-run=client -oyaml | kubectl apply -f -
fi



VALUES_BASE_YAML=$BASE_PATH/values.yaml
VALUES_ENV_YAML=$BASE_PATH/values.$LOWERCASE_ENV.yaml


helm template $PROJECT_NAME -n $NAMESPACE \
              -f $VALUES_BASE_YAML \
              -f $VALUES_ENV_YAML \
              $BASE_PATH > $BASE_PATH/deploy-template.yaml


kubectl apply -f $BASE_PATH/deploy-template.yaml -n $NAMESPACE
if [ "$?" != "0" ]; then
  exit 1
fi

echo "app deployed!"
