FROM alpine:3.18

ENV KUBECTL_VERSION="1.25.9" \
    KUBECTL_ARCHITECTURE="amd64" \
    NODE_ENV=production \
    NPM_CONFIG_PREFIX=/home/node/.npm-global \
    PATH=$PATH:/home/node/.npm-global/bin:/home/node/node_modules/.bin:$PATH \
    BUILD_DIR=/app/src/app/

RUN apk add --update jq libressl-dev gzip zlib-dev curl \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/${KUBECTL_ARCHITECTURE}/kubectl \
    && mv kubectl /usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl

COPY scripts/users.sh /users.sh

CMD ["sh", "/users.sh"]
