#!/bin/sh


while true
do
  USERS=$(kubectl get secrets $CONFIG_OBJECT_NAME -n $POD_NAMESPACE -ojsonpath='{.data.users}' | base64 -d)

  # echo "$USERS" 
  # | jq -r 'keys | .[]'

  for USER in $(echo $USERS | sed 's/,/ /g')
  do
    USERNAME=$(echo $USER | awk -F':' '{ print $1 }')
    PASSWORD=$(echo $USER | awk -F':' '{ print $2 }')

    # https://www.rabbitmq.com/access-control.html
    EXISTING_USERS=$(kubectl exec $MAIN_BROKER_POD_NAME -c rabbitmq -n $POD_NAMESPACE -- rabbitmqctl list_users --formatter=json | jq -r ".[] | .user")
    if [ ! -z "${EXISTING_USERS##*"$USERNAME"*}" ]; then
      echo "creating user $USERNAME"
      # CONFIG_PERMISSION="^client-.*"
      # if [ "$USERNAME" = "provo_api" ]; then
      #   CONFIG_PERMISSION="^controller-.*"
      # fi
      CONFIG_PERMISSION=".*"
      # rabbitmqctl [--node <node>] [--longnames] [--quiet] set_permissions [--vhost <vhost>] <username> <conf> <write> <read>
      kubectl exec $MAIN_BROKER_POD_NAME -c rabbitmq -n $POD_NAMESPACE -- sh -c "rabbitmqctl add_user '$USERNAME' '$PASSWORD' && rabbitmqctl set_permissions --vhost \"/\" \"$USERNAME\" \"$CONFIG_PERMISSION\" \".*\" \".*\""
    fi

    # rabbitmqctl list_users --formatter=json
    # rabbitmqctl add_user 'username' '9a55f70a841f18b97c3a7db939b7adc9e34a0f1d'
    # rabbitmqctl delete_user 'username'
    # rabbitmqctl set_permissions -p "custom-vhost" "username" ".*" ".*" ".*"
  done

  sleep 15
done
