# What is this?
A simple rabbitmq broker instance for testing purposes
- https://www.rabbitmq.com/monitoring.html#health-checks


# Official Provo website
https://provo.remolutions.com/

[![alt text](https://provo.remolutions.com/assets/logos/Provo_64p.png "Provo")](https://provo.remolutions.com/)




# How to install?
```shell
# export KUBECONFIG=$HOME/.kube/darkside-cluster
sh install.sh staging

sh install.sh prod


```


# How to remove?
```shell
# export KUBECONFIG=$HOME/.kube/darkside-cluster
# sh remove.sh staging
# sh remove.sh prod

```



# How to get started?
- https://hub.docker.com/r/bitnami/rabbitmq

```shell
sh launch.sh

```


# How to start a client and test the broker?
- https://amqp-node.github.io/amqplib/channel_api.html


# How to obeserve topics/channels
- visit: http://localhost:15672/


# How to prune all docker images
```shell
docker system prune -af --volumes

# in elevated powershell
optimize-vhd -Path “C:\Users\Impulse\AppData\Local\Docker\wsl\data\ext4.vhdx” -Mode full

```



## Provo Discord Server
- https://discord.gg/NJTFTgNvzG



## Donation/Support
If you like my work and want to support further development or just to spend me a coffee please

[![alt text](https://i.imgur.com/Y0XkUcd.png "Paypal $")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S3WQNNSVY8VAL)

[![alt text](https://i.imgur.com/xezX26q.png "Paypal €")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VQRPA46YADD9J)

